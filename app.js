const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const app = express();
const authRoutes = require('./routers/authRouter')
const userRoutes = require('./routers/userRouter');
const truckRoutes = require('./routers/truckRouter');
const loadRoutes = require('./routers/loadRouter')

dotenv.config();
const port = process.env.PORT;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(authRoutes);
app.use(userRoutes);
app.use(truckRoutes);
app.use(loadRoutes);

mongoose
    .connect('mongodb+srv://BK_user:12344321@cluster0.g649y.mongodb.net/hw3')
    .then((result) => {
        app.listen(port);
        console.log(`Server running at http://localhost:${port}/`)
    })
    .catch((err) => {
        console.log(err);
        throw err;
    });