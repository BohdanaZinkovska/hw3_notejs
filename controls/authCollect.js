const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
//const validationResult = require('express-validator')
const User = require('../models/User');

exports.postRegister = async(req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const role = req.body.role;
    console.log(email, password, role)

    const isUser = await User.findOne({ email: email });
    if (isUser) {
        return res.status(400).json({ message: 'User Already Exist. Please Login' });
    }
    const hashPass = bcrypt.hashSync(password, 10);
    const user = new User({
        email: email,
        password: hashPass,
        role: role,
        createdDate: new Date(),
    });
    await user.save();
    return res.status(200).json({ message: 'Success! User created!' });
};

exports.postLogin = async(req, res) => {
    const username = req.body.email;
    const password = req.body.password;

    if (!username || !password) {
        return res.status(400).json({ message: 'Fill all lines' });
    }

    const user = await User.findOne({ email: username });
    console.log(user);
    if (!user) {
        res.status(400).json({ message: "no such User" });
    }
    bcrypt.compare(password, user.password)
        .then((valid) => {
            if (!valid) {
                return res.status(400).json({ message: 'Wrong Username and Password' });
            } else {
                const token = jwt.sign({
                        user_id: user._id,
                        email: username,
                        role: user.role,
                    },
                    process.env.TOKEN_SECRET, {
                        expiresIn: '12h',
                    }
                );
                return res.status(200).json({ jwt_token: token })
            }

        })
        .catch((err) => {
            console.log(err);
        });

};

exports.forgotPassword = (req, res) => {
    try {
        const email = req.body.email;
        if (!email) {
            return res.status(400).json({ message: 'Invalid email' });
        } else {
            return res
                .status(200)
                .json({ message: 'New password sent to your email' });
        }
    } catch (error) {
        console.log(error);
    }
}