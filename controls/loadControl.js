const Load = require('../models/load');
const Truck = require('../models/trucks');
const changeStateOfLoad = () => {
    switch (state) {
        case 'En route to Pick Up':
            return 'Picked Up';
        case 'Picked Up':
            return 'En route to delivery';
        case 'En route to delivery':
            return 'Deliveried';
        default:
            return 'En route to Pick Up';
    }
};


exports.postLoad = (req, res) => {
    const user = req.user;
    const loadData = req.body;

    if (user.role !== "SHIPPER") {
        return res.status(400).json({ message: 'User is not a shipper' });
    }
    Load.create({
            created_by: user.user_id,
            name: loadData.name,
            payload: loadData.payload,
            pickup_address: loadData.pickup_address,
            delivery_address: loadData.delivery_address,
            dimensions: loadData.dimensions,
        })
        .then(() => {
            return res.status(200).json({ message: 'Load created successfully' });
        })
        .catch((err) => {
            console.log(err);
        })
}

exports.getLoads = (req, res) => {
    Load.find()
        .then((loads) => {
            return res.status(200).json({ loads: loads });
        })
        .catch((err) => {
            console.log(err);
        });
}

exports.postLoadById = async(req, res) => {
    const user = req.user;
    const loadId = req.params.id;
    if (user.role !== 'SHIPPER') {
        return res.status(400).json({ message: 'User is not a shipper' });
    } else if (!loadId) {
        return res.status(400).json({ message: 'Provide load id' });
    }
    const load = await Load.findOne({ _id: loadId });
    const freeTruck = await Truck.findOne({ assigned_to: { $ne: null }, status: 'IS' });
    if (!freeTruck) {
        return res
            .status(400)
            .json({ message: 'No truck was found' });
    }
    if (freeTruck.payload < load.payload) {
        return res
            .status(400)
            .json({ message: 'Truck doenst have enough payload ' });
    } else if (
        freeTruck.width < load.dimensions.width ||
        freeTruck.length < load.dimensions.length ||
        freeTruck.height < load.dimensions.height
    ) {
        return res.status(400).json({ message: 'Truck does not have enough place ' });
    }
    Truck.updateOne({ freeTruck }, { status: 'OL' })
        .then(() => {
            Load.updateOne({ _id: loadId }, { assigned_to: freeTruck.assigned_to, status: 'ASSIGNED' }).then(() => {
                res.status(200).json({
                    message: 'Load posted successfully',
                    driver_found: true,
                });
            });
        })
        .catch((err) => {
            console.log(err);
        });
}

exports.changeState = (req, res) => {
    const user = req.user;
    if (user.role !== 'DRIVER') {
        return res.status(400).json({ message: 'User is not a driver' });
    }
    Load.findOne({ assigned_to: user.user_id })
        .then((result) => {
            if (!result || result.status === 'SHIPPED') {
                return res.status(400).json({ message: 'No active loads' });
            }
            const newState = changeStateOfLoad(result.state);
            let status = 'ASSIGNED';
            if (newState === 'Deliveried') {
                status = 'SHIPPED';

            }
            Load.updateOne({ result }, { state: newState, status: status }).then(() => {
                return res
                    .status(200)
                    .json({ message: `Load state changed to ${newState}` });
            });
        })
        .catch((err) => {
            console.log(err);
        })
}

exports.getActiveLoad = (req, res) => {
    const user = req.user;
    if (user.role !== 'DRIVER') {
        return res.status(400).json({ message: 'User is not a driver' });
    }
    Load.findOne({ assigned_to: user.user_id })
        .then((load) => {
            if (!load || load.status === 'SHIPPED') {
                return res.status(400).json({ message: 'No active loads' });
            }
            return res
                .status(200)
                .json({ load: load });
        })
        .catch((err) => {
            console.log(err);
        });
}

exports.getLoadById = (req, res) => {
    const loadId = req.params.id;
    Load.findOne({ _id: loadId })
        .then((load) => {
            if (!load) {
                return res.status(400).json({ message: 'No such load' });
            }
            return res.status(200).json({ load: load });
        })
        .catch((err) => {
            console.log(err);
        });
}

exports.deleteLoadById = (req, res) => {
    const user = req.user;
    const loadId = req.params.id;
    if (user.role !== 'SHIPPER') {
        return res.status(400).json({ message: 'User is not a shipper' });
    } else if (!loadId) {
        return res.status(400).json({ message: 'Provide load id' });
    }
    Load.deleteOne({ _id: loadId })
        .then(() => {
            return res.status(200).json({ message: 'Load deleted successfully' });
        })
        .catch((err) => {
            console.log(err);
        });
}

exports.updateLoad = (req, res) => {
    const user = req.user;
    const loadId = req.params.id;
    const newLoad = req.body;
    if (user.role !== 'SHIPPER') {
        return res.status(400).json({ message: 'User is not a shipper' });
    } else if (!loadId || !newLoad) {
        return res.status(400).json({ message: 'Provide load id and data' });
    }
    Load.findOne({ _id: loadId })
        .then((load) => {
            if (load) {
                Load.updateOne({ _id: loadId }, {
                        name: newLoad.name,
                        payload: newLoad.payload,
                        pickup_address: newLoad.pickup_address,
                        delivery_address: newLoad.delivery_address,
                        dimensions: newLoad.dimensions,
                    })
                    .then(() => {
                        return res
                            .status(200)
                            .json({ message: 'Load details changed successfully' });
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }
        })
        .catch((err) => {
            console.log(err);
        });
}