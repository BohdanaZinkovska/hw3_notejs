const Truck = require('../models/trucks')
const typeOfTruck = () => {
    switch (typeOfTruck) {
        case 'SPRINTER':
            return [(width = 300), (length = 250), (height = 170), (payload = 100)];
        case 'SMALL STRAIGHT':
            return [(width = 500), (length = 250), (height = 170), (payload = 200)];
        case 'LARGE STRAIGHT':
            return [(width = 700), (length = 350), (height = 200), (payload = 400)];
        default:
            return [(width = 300), (length = 250), (height = 170), (payload = 100)];
    }
};


exports.postTruck = (req, res, next) => {
    const type = req.body.type;
    const driver = req.user;
    console.log(driver);
    if (driver.role.toLowerCase() !== 'driver') {
        return res.status(400).json({ message: 'User is not a driver' });
    }

    const [width, length, height, payload] = typeOfTruck(type);
    Truck.create({
            created_by: driver.user_id,
            type: type,
            width: width,
            length: length,
            height: height,
            payload: payload,
            created_date: new Date(),
        })
        .then(() => {
            res.status(200).json({ message: 'Truck created successfully' });
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ message: 'Server error' });
        });
}

exports.getTrucks = (req, res, next) => {
    const driver = req.user;
    if (driver.role.toLowerCase() !== 'driver') {
        return res.status(400).json({ message: 'User is not a driver' });
    }
    Truck.find()
        .then((trucks) => {
            console.log(trucks);
            return res.status(200).json({ trucks: trucks });
        })
        .catch((err) => {
            console.log(err);
            return res.status(500).json({ message: 'Server error' });
        })
}

exports.getTruckById = (req, res, next) => {
    const driver = req.user;
    const id = req.params.id;
    if (driver.role.toLowerCase() !== 'driver') {
        return res.status(400).json({ message: 'User is not a driver' });
    } else if (!id) {
        return res.status(400).json({ message: 'Provide an id' });
    }
    Truck.findOne({ _id: id })
        .then((truck) => {
            return res.status(200).json(truck);
        })
        .catch((err) => {
            console.log(err);
            return res.status(500).json({ message: 'Server error' });
        });
}

exports.updateTruckById = (req, res, next) => {
    const driver = req.user;
    const id = req.params.id;
    const type = req.body.type;
    if (driver.role.toLowerCase() !== 'driver') {
        return res.status(400).json({ message: 'User is not a driver' });
    } else if (!id || !type) {
        return res.status(400).json({ message: 'Provide an id and type' });
    }
    Truck.findOne({ _id: id })
        .then((truck) => {
            if (truck) {
                Truck.updateOne({ _id: id }, { type: type })
                    .then((result) => {
                        return res
                            .status(200)
                            .json({ message: 'Truck details changed successfully' });
                    })
                    .catch((err) => {
                        console.log(err);
                        return res.status(500).json({ message: 'Server error' });
                    });
            }
        })
        .catch((err) => {
            console.log(err);
            return res.status(500).json({ message: 'Server error' });
        });
};

exports.deleteTruckById = (req, res, next) => {
    const driver = req.user;
    const id = req.params.id;
    if (driver.role.toLowerCase() !== 'driver') {
        return res.status(400).json({ message: 'User is not a driver' });
    }

    Truck.deleteOne({ _id: id })
        .then(() => {
            return res.status(200).json({ message: 'Truck deleted successfully' });
        })
        .catch((err) => {
            console.log(err);
            return res.status(500).json({ message: 'Server error' });
        });
};

exports.assignTruck = (req, res, next) => {
    const driver = req.user;
    const driverId = driver.user_id;
    const id = req.params.id;
    if (driver.role.toLowerCase() !== 'driver') {
        return res.status(400).json({ message: 'User is not a driver' });
    }
    Truck.findOne({ assigned_to: driverId })
        .then((result) => {
            if (result) {
                console.log(result);
                return res
                    .status(400)
                    .json({ message: 'Driver can assign only one truck' });
            } else {
                Truck.updateOne({ _id: id }, { assigned_to: driverId })
                    .then(() => {
                        console.log(driverId);
                        return res
                            .status(200)
                            .json({ message: 'Truck assigned successfully' });
                    })
                    .catch((err) => {
                        console.log(err);
                        return res.status(500).json({ message: 'Server error' });
                    });
            }
        })
        .catch((err) => {
            console.log(err);
            res.status(500).json({ message: 'Server error' });
        });
}