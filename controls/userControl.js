const User = require('../models/User');

exports.getProfileInfo = (req, res) => {
    try {
        const userData = req.user;
        if (!userData) {
            return res.status(400).json({ message: 'No data' });
        }
        res.status(200).json({
            user: {
                _id: userData.user_id,
                role: userData.role,
                email: userData.email,
                created_date: userData.createdDate,
            },
        });
    } catch (error) {
        console.log(error);
    }
}

exports.deleteProfile = (req, res) => {
    const userId = req.user.user_id;
    if (!userId) {
        return res.status(400).json('No such profile');
    }
    User.deleteOne({ _id: userId })
        .then(() => {
            return res.status(200).json({ message: 'Profile deleted successfully' });
        })
        .catch((err) => {
            console.log(err);
        });
}