const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const truckSchema = new Schema({
    created_by: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    assigned_to: { type: Schema.Types.ObjectId, ref: 'User', default: null },
    type: {
        type: String,
        required: true,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    },
    status: { type: String, enum: ['IS', 'OL'], default: 'IS' },
    width: { type: Number, required: true },
    length: { type: Number, required: true },
    height: { type: Number, required: true },
    payload: { type: Number, required: true },
    created_date: { type: String, required: true },
});

module.exports = mongoose.model('Truck', truckSchema);