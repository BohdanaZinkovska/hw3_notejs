const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const loadSchema = new Schema({
    created_by: { type: Schema.Types.ObjectId, required: true, ref: 'User' },
    assigned_to: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        default: null,
    },
    status: {
        type: String,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        required: true,
        default: 'NEW',
    },
    state: {
        type: String,
        enum: [
            'En route to Pick Up',
            'Picked Up',
            'En route to delivery',
            'Deliveried',
        ],
        default: 'En route to Pick Up',
    },
    name: { type: String, required: true },
    payload: { type: Number, required: true },
    pickup_address: { type: String, required: true },
    delivery_address: { type: String, required: true },
    dimensions: {
        width: { type: Number, required: true },
        length: { type: Number, required: true },
        height: { type: Number, required: true },
    }
});

module.exports = mongoose.model('Load', loadSchema);