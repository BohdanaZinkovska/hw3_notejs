const jwt = require('jsonwebtoken');

const authorization = (req, res, next) => {
    const token = req.headers['authorization'];
    console.log(token);
    if (!token) {
        return res.status(400).json({ message: 'Token was not found' });
    }
    try {
        const data = jwt.verify(token.split(' ')[1], process.env.TOKEN_SECRET);
        if (data) {
            req.user = data;
            return next();
        } else {
            return res.status(400).json({ message: 'Token is not valid' })
        }

    } catch (err) {
        console.log(err);
    }
};

module.exports = authorization;