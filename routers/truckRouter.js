const express = require('express');
const truckController = require('../controls/truckControl');
const auth = require('../middleware/authMiddleware');
const router = express.Router();

router.post('/api/trucks', auth, truckController.postTruck);
router.post('/api/trucks/:id/assign', auth, truckController.assignTruck);
router.get('/api/trucks', auth, truckController.getTrucks);
router.get('/api/trucks/:id', auth, truckController.getTruckById);
router.put('/api/trucks/:id', auth, truckController.updateTruckById);
router.delete('/api/trucks/:id', auth, truckController.deleteTruckById);

module.exports = router;