const express = require('express');
const router = express.Router();
const auth = require('../middleware/authMiddleware');
const loadController = require('../controls/loadControl');

router.post('/api/loads', auth, loadController.postLoad);
router.post('/api/loads/:id/post', auth, loadController.postLoadById);
router.patch('/api/loads/active/state', auth, loadController.changeState);

router.get('/api/loads/active', auth, loadController.getActiveLoad);
router.get('/api/loads', auth, loadController.getLoads);
router.get('/api/loads/:id', auth, loadController.getLoadById);

router.delete('/api/loads/:id', auth, loadController.deleteLoadById);
router.put('/api/loads/:id', auth, loadController.updateLoad);

module.exports = router;