const express = require('express');
const userControl = require('../controls/userControl');
const auth = require('../middleware/authMiddleware')
const router = express.Router();

router.get('/api/users/me', auth, userControl.getProfileInfo);
router.delete('/api/users/me', auth, userControl.deleteProfile);

module.exports = router;