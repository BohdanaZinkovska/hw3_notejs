const express = require('express');
const authController = require('../controls/authCollect')
const router = express.Router();

router.post('/api/auth/register', authController.postRegister);

router.post('/api/auth/login', authController.postLogin);

router.post('/api/auth/forgot_password', authController.forgotPassword);

module.exports = router;